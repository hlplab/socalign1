#
# Author: Andrew Watts <awatts@bcs.rochester.edu>
#
# Copyright (c) 2014, Andrew Watts and
#        the University of Rochester BCS Department
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from app import db
from werkzeug.exceptions import abort

__all__ = ['Worker', 'Experiment', 'Assignment', 'HITTypeId', 'HITId',
           'TrialList', 'NotificationEvent']


class MyMixin(object):

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def get_one_or_create(cls, create_method='',
                          create_method_kwargs=None,
                          **kwargs):
        """
        Get the db instance of the kwarg specified object or create
        if necessary. Returns a tuple of the object and whether it
        already existed (True) or was created (False)
        Modified from:
        http://skien.cc/blog/2014/01/15/sqlalchemy-and-race-conditions-implementing/
        """
        try:
            return cls.query.filter_by(**kwargs).one(), True
        except NoResultFound:
            kwargs.update(create_method_kwargs or {})
            created = getattr(cls, create_method, cls)(**kwargs)
            try:
                db.session.add(created)
                db.session.commit()
                return created, False
            except IntegrityError:
                db.session.rollback()
                return cls.query.filter_by(**kwargs).one(), True
        except MultipleResultsFound:
            # FIXME: Multiples should never happen
            # In the case they do it should somehow warn so it can get fixed
            # short term solution is to just always return the first one so
            # everything gets assigned that one and it's easier to delete dups
            return cls.query.filter_by(**kwargs).first(), True

    @classmethod
    def get_object_or_404(cls, **kwargs):
        try:
            return cls.query.filter_by(**kwargs).one()
        except (NoResultFound, MultipleResultsFound) as e:
            abort(404, {'message': e})


class Worker(db.Model, MyMixin):
    """
    Representation of an mturk worker and what list they were assigned to
    """
    workerid = db.Column(db.String(32), unique=True)

    def __repr__(self):
        return '<Worker: "{}">'.format(self.workerid)


# class Protocol(db.Model, MyMixin):
#     """
#     RSRB protocol.
#     """
#     name = db.Column(db.String(128), unique=True)
#     number = db.Column(db.String(24), unique=True)


class Experiment(db.Model, MyMixin):
    """
    Representation of an experiment
    """
    name = db.Column(db.String(32), unique=True)
    # protocol_id = db.Column(db.Integer, db.ForeignKey('protocol.id'))
    # protocol = db.relationship('Protocol', backref='experiments')

    def __repr__(self):
        return 'Experiment: "{}"'.format(self.name)


class Assignment(db.Model, MyMixin):
    """
    A MTurk HIT assignment
    """
    assignmentid = db.Column(db.String(32), unique=True)
    state = db.Column(db.Enum(u'InProcess', u'Submitted', u'Abandoned', u'Returned', u'Rejected', u'Approved',
                      name=u'AssignmentState'), default=u'InProcess')
    list_id = db.Column(db.Integer, db.ForeignKey('triallist.id'))
    # list_name = db.Column(db.String(32), db.ForeignKey('triallist.name'))
    triallist = db.relationship('TrialList', backref='assignments', foreign_keys=[list_id])
    worker_id = db.Column(db.Integer, db.ForeignKey('worker.id'))
    worker = db.relationship('Worker', backref='assignments')
    experiment_id = db.Column(db.Integer, db.ForeignKey('experiment.id'))
    experiment = db.relationship('Experiment', backref='assignments', foreign_keys=[experiment_id])

    def __repr__(self):
        try:
            return 'Assignment: "{}" TrialList: {}::{}'.format(self.assignmentid, self.experiment.name, self.triallist.name)
        except AttributeError:
            return 'Assignment: "{}"'.format(self.assignmentid)


class HITTypeId(db.Model, MyMixin):
    """
    An Amazon Mechanical Turk HITTypeId
    """
    hittypeid = db.Column(db.String(32))

    def __repr__(self):
        return 'HITTypeId: "{}"'.format(self.hittypeid)


class HITId(db.Model, MyMixin):
    """
    An Amazon Mechanical Turk HITId
    """
    hitid = db.Column(db.String(32))
    hittypeid_id = db.Column(db.Integer, db.ForeignKey('hittypeid.id'))
    hittypeid = db.relationship('HITTypeId', backref='HITS')

    def __repr__(self):
        return 'HITId: "{}"'.format(self.hitid)


class TrialList(db.Model, MyMixin):
    """
    Which list workers are on
    """

    name = db.Column(db.String(32))
    number = db.Column(db.Integer)
    experiment_id = db.Column(db.Integer, db.ForeignKey('experiment.id'))
    experiment = db.relationship('Experiment', backref='lists', foreign_keys=[experiment_id])

    db.UniqueConstraint(experiment_id, name)

    def __repr__(self):
        return '<TrialList: "{}::{}">'.format(self.experiment, self.name)


class NotificationEvent(db.Model, MyMixin):
    """
    An Amazon Mechanical Turk notification event
    """

    event_type = db.Column(
        db.Enum(u'AssignmentAccepted', u'AssignmentAbandoned',
                u'AssignmentReturned', u'AssignmentSubmitted',
                u'HITReviewable', u'HITExpired', u'Ping',
                # default='',
                name='EventType'))
    event_time = db.Column(db.DateTime(timezone=True))
    eventdocid = db.Column(db.String(48))
    assignment_id = db.Column(db.Integer, db.ForeignKey('assignment.id'), nullable=True)
    assignment = db.relationship('Assignment', backref='events')
    hitid_id = db.Column(db.Integer, db.ForeignKey('hitid.id'))
    hitid = db.relationship('HITId', backref='events')
    hittypeid_id = db.Column(db.Integer, db.ForeignKey('hittypeid.id'))
    hittypeid = db.relationship('HITTypeId', backref='events')

    def __repr__(self):
        return 'Event: "{}::{}"'.format(self.event_type, self.event_time)
