/*
  Survey validation
*/
$('#page1 button.next').on('click', function() {
  if ($('#page1 input, select').valid()) {
    $('#page1').hide();
    $('#experiment_progress').val($('#experiment_progress').val() + 0.25);
    $('#page2').show(function() {$('.surveyhead:visible')[0].scrollIntoView();});
  } else {
    $('#page1 input, select:blank').first()[0].scrollIntoView();
  }
});

$('#page2 button.next').on('click', function() {
  if ($('#page2 input, select').valid()) {
    $('#page2').hide();
    $('#experiment_progress').val($('#experiment_progress').val() + 0.25);
    $('#page3').show(function() {$('.surveyhead:visible')[0].scrollIntoView();});
  } else {
    $('#page2 input, select:blank').first()[0].scrollIntoView();
  }
});

$('#page3 button.next').on('click', function() {
  if ($('#page3 input, select').valid()) {
    $('#page3').hide();
    $('#experiment_progress').val($('#experiment_progress').val() + 0.25);
    $('#page4').show(function() {$('.surveyhead:visible')[0].scrollIntoView();});
  } else {
    $('#page3 input, select:blank').first()[0].scrollIntoView();
  }
});

$('#page4 button.next').on('click', function() {
  if ($('#page4 input, select').valid()) {
    $('#page4').hide();
    $('#experiment_progress').val($('#experiment_progress').val() + 0.25);
    $('#rsrb').show();
  } else {
    $('#page4 input, select:blank').first()[0].scrollIntoView();
  }
});

$('#endrsrb').on('click', function() {
  $('#rsrb').hide();
  $('#experiment_progress').val(4);
  wrapup();
});

var wrapup = function() {
    //$('#debriefing').show();
    $('#phase_label').text('Comment and submission');
    $('#comment').show(function() {$('#commentarea').focus();});
    $('#submit').show(function() {
        $(this).removeAttr('disabled');
        finished = true;
    });
};
