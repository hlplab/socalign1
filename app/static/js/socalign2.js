function skipToTrials() {
  $('#instructions').hide();
  $('#experiment_progress').val(1);
  $('.block').first().show(0);
  $('#phase_label').text('Phase 2: Sentence completion task');
}

$(document).ready(function() {
  var navigator = window.navigator;
  var trialStarted;
  $(':checked').removeAttr('checked');
  $('input[name="browserid"]').val(navigator.userAgent);


  var listener = new window.keypress.Listener();
  listener.simple_combo('f', function() {
    console.log('y pressed');
    $('button.yes:visible').click();
  });
  listener.simple_combo('shift f', function() {
    console.log('Yes pressed');
    $('button.yes:visible').click();
  });
  listener.simple_combo('j', function() {
    console.log('n pressed ');
      $('button.no:visible').click();
  });
  listener.simple_combo('shift j', function() {
    console.log('No pressed');
      $('button.no:visible').click();
  });
  listener.stop_listening(); // Don't want to listen outside 2AFC trials


  var block1_trials = $('.block').first().children('.testtrial').map(
    function() {
      return this.id;
    }).toArray();

  var block2_trials = $('.block:nth-of-type(2)').children('.testtrial').map(
    function() {
      return this.id;
    }).toArray();

  var block1_size = block1_trials.length;
  var block2_size = block2_trials.length;
  var total_trials = block1_size + block2_size;
  var trial_update = 1 / total_trials;

  var trial_vals = {};

  var current_block = 1;
  var max_blocks = 2;

  try {
    navigator.getUserMedia = (
    navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);
    audio_context = new AudioContext();
    console.log('Audio context set up.');

    navigator.getUserMedia({audio: true},
        function(stream) {
          //we have a mic, set up the recorder and start the hit.
            startRecorder(stream);
          },
        function(err) {
          //maybe also issue alert that this hit should not be accepted as there was an issue getting the audio stream setup?
          //For now if the audio stream doesn't work than the hit doesn't advance but with no error message
            console.log('The following error occurred: ' + err.name);
            alert('You must allow access to your microphone to do this HIT!');
            $('#endinstr').attr('disabled', 'disabled');
          }
    );

    } catch (e) {
        alert('There is no audio recording support in this browser, do not accept this HIT!');
    }

  if (Modernizr.audio) {
      $('#instructions').show(0);

      $('#voladjust').on('play', function() {this.volume = 1;});
      $('#voladjust').on('volumechange', function() {this.volume = 1;});
      $('#voladjust').on('ended', function() {
        this.currentTime = 0; this.pause();
        $('.micsetup').show();
        displayAudioMeter();
      });

      $('#exposure audio').on('play', function() {
        console.log('Audio playing');
      });

      $('#exposure audio').on('ended', function() {
          console.log('Audio ended');
          $('#headphones').hide();
          $('#exposure').hide();
          $('#experiment_progress').val(1);
          $('.block').first().show(0);
          $('#phase_label').text('Phase 2: Sentence completion task');
      });

      var v = document.getElementById('exposure_audio');
      v.addEventListener('timeupdate', function() {
        $('#experiment_progress').val(v.currentTime / v.duration);
      });
  }

  $('#micadjusted').on('change', function() {
    if ($(this).is(':checked')) {
      $('#endaudiosetup').removeAttr('disabled');
    } else {
      $('#endaudiosetup').attr('disabled', 'disabled');
    }
  });

  $('button#endinstr').on('click', function() {
    $('#instructions').hide();
    $('#exposure').show(function() {
        $('#phase_label').text('Phase 1: Listen to a political opinion statement');
        $('#exposure audio')[0].volume = 1;
        $('#exposure audio')[0].play();
        $('#headphones').show(0);
    });
  });

  $('.yesno').on('click', function() {
    var itemId = $(this).parent().attr('id');
    trial_vals[itemId] = {
      choice: this.value,
      startTime: trialStarted.toISOString(),
      stopTime: new Date().toISOString()
    };
    listener.stop_listening();
    nextTrial('2AFC');
  });


  $('.stoprecord').on('click', function() {
    recorder.stop();
    $('#microphone').hide();
    //$('#cloud').show();
    //$('.progresstext').show();
    var upload_file = $('.testtrial:visible a').attr('href');
    console.log('Current file: ' + upload_file);
    recorder.exportWAV(function(blob) {
      // We're going to try to clear the buffer and proceed while it's uploading
      recorder.clear();
      nextTrial('Record');
      // http://jsfiddle.net/GvdSy/
      $.ajax({
        xhr: function() {
          var xhr = new window.XMLHttpRequest();
          xhr.upload.addEventListener('progress', function(evt) {
            if (evt.lengthComputable) {
              var percentComplete = evt.loaded / evt.total;
              //console.log(percentComplete);
              // $('.progress').css({
              //   width: percentComplete * 100 + '%'
              // });
              // $('.progresstext').text((percentComplete * 100).toFixed(2) + '%');
              // if (percentComplete === 1) {
              //   $('.progress').addClass('hide');
              //   $('.progresstext').text('0%').hide(0);
              // }
            }
          }, false);
          xhr.addEventListener('progress', function(evt) {
            if (evt.lengthComputable) {
              var percentComplete = evt.loaded / evt.total;
              console.log(percentComplete);
              // $('.progress').css({
              //   width: percentComplete * 100 + '%'
              // });
            }
          }, false);
          return xhr;
        },
        type: 'PUT',
        //url: $('.testtrial:visible a').attr('href'),
        url: upload_file,
        data: blob,
        processData: false,
        contentType: 'audio/x-wav',
        success: function(data, textStatus, jqXHR) {
          //recorder.clear();
          // $('#cloud').hide();
          // $('.progress').css({
          //   width: 0
          // }).removeClass('hide');
          console.log(textStatus, data);
          //nextTrial('Record');
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
      });
    });
  });

  $('.startblock.2AFC').on('click', function() {
    $(this).parent().hide();
    $(this).parent().siblings('.testtrial').first().show(0, function() {
      trialStarted = new Date();
      listener.listen();
    });
  });

  $('.startblock.sentence_completion').on('click', function() {
    $(this).parent().hide();
    $(this).parent().siblings('.testtrial').first().show(0, function() {
      $('.testtrial:visible .fixcross').delay(500).hide(0)
                                       .siblings('.stimsentence, .stoprecord')
                                       .delay(500).show(0);
      recorder.record();
      $('#microphone').show();
    });
  });

  var nextTrial = function(trialtype) {
    var trialblock;
    if (current_block === 1) {
      trialblock = block1_trials;
    } else {
      trialblock = block2_trials;
    }
    var current = '#' + trialblock.shift();
    $(current).hide();
    var pval = $('#experiment_progress').val();
    $('#experiment_progress').val(pval + trial_update);
    if (trialblock.length === 0) { // last trial in block
      $('.block:nth-of-type(' + current_block + ')').hide();
      current_block++;
      if (current_block === 2) {
        $('#phase_label').text('Phase 3: Memory task');
      }
      if (current_block <= max_blocks) {
        $('.block:nth-of-type(' + current_block + ')').show(0);
      } else {
        $('[name="trials"]').val(JSON.stringify(trial_vals));
        $('#page1').show(0);
        $('#phase_label').text('Phase 4: Survey');
      }
    } else { // normal trial
      var next = '#' + trialblock[0];
      $(next).show(0, function() {
        if (trialtype === '2AFC') {
          trialStarted = new Date();
          listener.listen();
        } else {
          $('.testtrial:visible .fixcross').delay(500).hide(0)
                                           .siblings('.stimsentence, .stoprecord')
                                           .delay(500).show(0);
          recorder.record();
          $('#microphone').show();
        }
      });
    }

  };

});
