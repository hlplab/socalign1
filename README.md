# SocAlign1 #

## External Python dependancies: ##
All of these should be installable with [easy\_install or pip](http://pythonhosted.org/distribute/)

* [Six](https://pythonhosted.org/six/)
* [Flask](http://flask.pocoo.org/)
* [Flask SQLAlchemy](http://flask-sqlalchemy.pocoo.org/)
* [Flask Assets](https://flask-assets.readthedocs.org/)

## External JS dependencies: ##
* [Modernizr](http://modernizr.com/)

## Other external dependencies: ##
 * [clean-css](https://github.com/jakubpawlowicz/clean-css)
 * [UglifyJS2](https://github.com/mishoo/UglifyJS2)

## Setup ##

### Local Test Server ###
Make a directory called "stimuli" and copy all the stimuli for the experiment
into it. Run the command `db_init.py` to create the test SQLite database. Copy
"example.cfg" to "expt.cfg" and edit as necessary.

If you run the command `python run.py`, Flask will serve the
experiment at <http://127.0.0.1:5000/> Be sure to
pass it 'assignmentId', 'hitId', and 'workerId' parameters. Use whatever values
you want for testing. These are the parameters that MTurk will pass the script.
 If you pass the additional parameter of  "debug=1", then the rendered version
 of the page will allow you to reset your place in the experiment when you
resume, and there will be controls on the audio file, allowing you to scrub
through quickly instead of listening to the whole thing. If you set the value
of 'assignmentId' to 'ASSIGNMENT\_ID\_NOT\_AVAILABLE', it will serve the
preview version of the experiment a worker browsing on MTurk would see instead
of the real thing.

### Apache ###
Make a socalign1.wsgi file like this:

    :::python
    import sys

    path = '/usr/local/mturk-apps/socalign1'
    if path not in sys.path:
        sys.path.append(path)
    import SocAlign1

    application = SocAlign1.SocAlign1Server()

assuming that you plan to put your MTurk experiment WSGIs in /usr/local/mturk-apps/
Make a similar one for WavUploader.

In your Apache config add something like this:

    WSGIScriptAliasMatch ^/mturk/experiments/([^/]+) /usr/local/mturk/$1.wsgi

assuming that you put the WSGI files in /usr/local/mturk/

Copy the file "example.cfg" to "expt.cfg" and edit as necessary, but to match
your Apache setup instead.

## Citing ##
If parts of this code are of use to you, we would appreciate if you could
acknowledge our funding sources and spread the word: "This code was developed
by Andrew Watts, funded by NSF IIS-1150028 to T.F. Jaeger and distributed
through HLP Lab. For details, see Weatherholtz, K., and Campbell-Kibler, K.,
and Jaeger, T. F. 2012. Syntactic Alignment is Mediated by Social Perception
and Conflict Management. 19th Architecture and Mechanisms for Language
Processing 2012, Riva del Garda, Italy."
